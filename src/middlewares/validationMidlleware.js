const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .min(4)
        .max(20)
        .required(),
    role: Joi.string()
        .valid('DRIVER', 'SHIPPER')
        .optional(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (error) {
    // next(error);
    return res.status(403).send(
        {message: error.message},
    );
  }
};

module.exports = {
  registrationValidator,
};
