const shipperMiddleware = (req, res, next) => {
  const {role} = req.user;

  if (role !== 'SHIPPER') {
    return res.status(400).json({
      message: 'Do not have the required permissions',
    });
  }
  next();
};

module.exports = {
  shipperMiddleware,
};
