const driverMiddleware = (req, res, next) => {
  const {role} = req.user;
  //   console.log('req: ', req);
  //   console.log('role: ', role);
  if (role !== 'DRIVER') {
    return res.status(400).json({
      message: 'Do not have the required permissions',
    });
  }
  next();
};

module.exports = {
  driverMiddleware,
};
