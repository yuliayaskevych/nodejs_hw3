const express = require('express');
const router = new express.Router();
const {
  getTrucks,
  addTruck,
  getTruckById,
  changeType,
  assignTruck,
  deleteTruck,
} = require('../services/trucksService');

router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const trucks = await getTrucks(userId);
    res.status(200).json({trucks});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/', async (req, res) => {
  try {
    const {userId} = req.user;
    // console.log(userId);
    await addTruck(userId, req.body);

    res.status(200).json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const {userId} = req.user;
    const truck = await getTruckById(userId, req.params.id);

    res.status(200).json({truck});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.put('/:id', async (req, res) => {
  try {
    const {userId} = req.user;

    await changeType(userId, req.params.id, req.body);
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/:id/assign', async (req, res) => {
  try {
    const {userId} = req.user;

    await assignTruck(userId, req.params.id);
    res.status(200).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const {userId} = req.user;

    await deleteTruck(userId, req.params.id);

    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

module.exports = {
  trucksController: router,
};
