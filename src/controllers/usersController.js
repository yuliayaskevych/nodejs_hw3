const express = require('express');
const router = new express.Router();
const {
  getUser,
  deleteUser,
  changePassword,
} = require('../services/usersService');

router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const user = await getUser(userId);
    res.status(200).json({user});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.delete('/', async (req, res) => {
  try {
    const {userId} = req.user;

    await deleteUser(userId);

    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});


router.patch('/password', async (req, res) => {
  try {
    const {userId} = req.user;
    const {oldPassword, newPassword} = req.body;

    await changePassword(userId, oldPassword, newPassword);

    res.status(200).json({message: 'Password changed successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});


module.exports = {
  usersController: router,
};
