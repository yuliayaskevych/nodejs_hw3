const express = require('express');
const router = new express.Router();

const {
  registration,
  loginUser,
  forgotPassword,
} = require('../services/authService');

const {registrationValidator} = require('../middlewares/validationMidlleware');

router.post('/register', registrationValidator, async (req, res) => {
  try {
    const {
      role,
      email,
      password,
    } = req.body;
    await registration({role, email, password});

    res.json({message: 'Profile created successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/login', async (req, res) => {
  try {
    const {
      role,
      email,
      password,
    } = req.body;
    const token = await loginUser({role, email, password});

    res.json({jwt_token: token});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/forgot_password', async (req, res) => {
  try {
    const {
      email,
    } = req.body;
    await forgotPassword({email});

    res.json({message: 'New password sent to your email address'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

module.exports = {
  authController: router,
};
