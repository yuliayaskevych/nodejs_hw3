const express = require('express');
const router = new express.Router();
const {shipperMiddleware} = require('../middlewares/shipperMiddleware');
const {driverMiddleware} = require('../middlewares/driverMiddleware');


const {
  getLoads,
  addLoad,
  getActiveLoad,
  changeStateLoad,
  getLoadById,
  updateLoadById,
  deleteLoad,
  postLoadById,
  shippingInfoLoadById,
} = require('../services/loadService');

router.get('/', async (req, res) => {
  try {
    const {userId, role, status} = req.user;
    const loads = await getLoads(userId, role, status);
    res.status(200).json({loads});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/', shipperMiddleware, async (req, res) => {
  try {
    const {userId} = req.user;
    await addLoad(userId, req.body);
    res.status(200).json({message: 'Load created successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.get('/active', driverMiddleware, async (req, res) => {
  try {
    const {userId} = req.user;
    const load = await getActiveLoad(userId);
    res.status(200).json({load});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});
// problem
router.patch('/active/state', driverMiddleware, async (req, res) => {
  try {
    const {userId} = req.user;
    const newState = await changeStateLoad(userId);
    res.status(200).json({message: `Load state changed to '${newState}'`});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const {userId} = req.user;
    const load = await getLoadById(userId, req.params.id);
    res.status(200).json({load});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.put('/:id', shipperMiddleware, async (req, res) => {
  try {
    const {userId} = req.user;
    await updateLoadById(userId, req.params.id, req.body);
    res.status(200).json({message: 'Load details changed successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.delete('/:id', shipperMiddleware, async (req, res) => {
  try {
    const {userId} = req.user;
    await deleteLoad(userId, req.params.id);
    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/:id/post', shipperMiddleware, async (req, res) => {
  try {
    // const {loadId} = req.params.id;
    const driverFound = await postLoadById(req.params.id);
    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: driverFound,
    });
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.get('/:id/shipping_info', shipperMiddleware, async (req, res) => {
  try {
    const {userId} = req.user;
    const {load, truck} = await shippingInfoLoadById(userId, req.params.id);
    res.status(200).json({load, truck});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

module.exports = {
  loadController: router,
};
