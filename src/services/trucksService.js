const {Truck} = require('../models/truckModel');

const getTrucks = async (userId) => {
  const trucks = await Truck.find({created_by: userId}, {__v: 0});
  if (!trucks) {
    throw new Error('No trucks with such id found');
  }
  return trucks;
};

const addTruck = async (userId, truckPayload) => {
  const truck = new Truck({...truckPayload, created_by: userId});
  await truck.save();
};

const getTruckById = async (userId, _id) => {
  const truck = await Truck.findOne({_id, created_by: userId}, {__v: 0});
  if (!truck) {
    throw new Error('No truck found');
  }
  return truck;
};

const changeType = async (userId, _id, {type}) => {
  const truck = await Truck.findOne({_id, created_by: userId});
  if (!truck) {
    throw new Error('No truck found');
  }
  truck.type = type;
  await truck.save();
};

const assignTruck = async (userId, _id) => {
  const truck = await Truck.findOne({_id, created_by: userId});
  if (!truck) {
    throw new Error('No truck found');
  }
  await Truck.updateOne({_id}, {
    $set: {
      assigned_to: userId,
    },
  });
  return truck;
};

const deleteTruck = async (userId, _id) => {
  const truck = await Truck.findOneAndDelete({_id, created_by: userId});
  if (!truck) {
    throw new Error('No truck found');
  }
  return truck;
};

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  changeType,
  assignTruck,
  deleteTruck,
};
