const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');
const newPassword = 'newPassword';
const registration = async ({role, email, password}) => {
  const user = new User({
    role,
    email,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

const loginUser = async ({email, password}) => {
  const user = await User.findOne({email});
  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
  }, 'secret');
  return token;
};

const forgotPassword = async ({email}) => {
  const user = await User.findOne({email});
  if (!user) {
    throw new Error('No email found');
  }
  await User.updateOne({email}, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });
  // const user = new User({
  //   role,
  //   email,
  //   password: await bcrypt.hash(password, 10),
  // });
  // await user.save();
};

module.exports = {
  registration,
  loginUser,
  forgotPassword,
};
