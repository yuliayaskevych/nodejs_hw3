const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {checkTruckLoad} = require('../utils/checkTruckLoad');
const {stateLoad} = require('../models/data');


const getLoads = async (userId, role, status, offset = 0, limit = 10) => {
  if (limit > 50) {
    limit = 50;
  }
  if (role === 'DRIVER' && (status === 'POSTED' || status === 'SHIPPED')) {
    const loads = await Load.find({assigned_to: userId, status})
        .skip(offset)
        .limit(limit);

    if (!loads) {
      throw new Error('No loads with such id found');
    }
    return loads;
  }
  if (role === 'SHIPPER') {
    const loads = await Load.find({created_by: userId})
        .skip(offset).limit(limit);

    if (!loads) {
      throw new Error('No loads with such id found');
    }
    return loads;
  }
};

const addLoad = async (userId, loadPayload) => {
  const load = new Load({...loadPayload, created_by: userId});
  await load.save();
};

const getActiveLoad = async (userId) => {
  const load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
  if (!load) {
    throw new Error('No active load found');
  }
  return load;
};
// problem
const changeStateLoad = async (userId) => {
  let load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
  if (!load) {
    throw new Error('No active load found');
  }
  if (load.status === 'SHIPPED') {
    throw new Error('Load is already shipped');
  }
  const index = stateLoad.indexOf(load.state);
  if (index !== stateLoad.length-1) {
    load.state = stateLoad[index+1];
    load.logs.message = stateLoad[index+1];
    load.logs.time = Date.now();
  }
  if (index === stateLoad.length-2) {
    load.status = 'SHIPPED';
  }
  await load.save();
  load = await Load.findOne({assigned_to: userId});

  return load.state;
};

const deleteLoad = async (userId, _id) => {
  const load = await Load.findOneAndDelete({_id, created_by: userId});
  if (!load) {
    throw new Error('No load found');
  }
  return load;
};
// new
const postLoadById = async (_id) => {
  const load = await Load.findOne({_id});
  if (!load) {
    throw new Error('No load found');
  }
  await load.updateOne({
    $set: {
      status: 'POSTED',
    },
  });

  const trucks = await Truck.find({
    status: 'IS',
    assigned_to: {$ne: null},
  });
  let driverFound = false;
  const truck = trucks?.find((truck)=>checkTruckLoad(truck, load));
  if (!truck) {
    await load.updateOne({
      $set: {
        status: 'NEW',
      },
    });
    throw new Error('No truck found');
  }

  await truck.updateOne({
    $set: {
      status: 'OL',
    },
  });

  driverFound = true;
  const driverId = truck.assigned_to;

  await load.updateOne({
    $set: {
      status: 'ASSIGNED',
      state: 'En route to Pick Up',
      assigned_to: driverId,
    },
    $push: {
      logs: {
        message: `Load assigned to driver with id ${driverId}`,
        time: new Date(Date.now()),
      },
    },
  });

  return driverFound;
};

const getLoadById = async (userId, _id) => {
  const load = await Load.findOne({_id, created_by: userId}, {__v: 0});
  if (!load) {
    throw new Error('No load found');
  }
  return load;
};

const updateLoadById = async (userId, _id, data) => {
  const load = await Load.findOneAndUpdate({_id, created_by: userId},
      {$set: data});
  if (!load) {
    throw new Error('No load found');
  }
};

const shippingInfoLoadById = async (userId, _id) => {
  const load = await Load.findOne({_id, created_by: userId}, {__v: 0});
  if (!load) {
    throw new Error('No load found');
  }
  const truck = await Truck.findOne({assigned_to: load.assigned_to}, {__v: 0});
  if (!truck) {
    throw new Error('No truck found');
  }
  return {load, truck};
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  changeStateLoad,
  deleteLoad,
  postLoadById,
  getLoadById,
  updateLoadById,
  shippingInfoLoadById,
};

