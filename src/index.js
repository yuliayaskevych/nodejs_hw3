const express = require('express');
// const path = require('path');
const morgan = require('morgan');
const app = express();
const mongoose = require('mongoose');

const {trucksController} = require('./controllers/trucksController');
const {usersController} = require('./controllers/usersController');
const {authController} = require('./controllers/authController');
const {loadController} = require('./controllers/loadController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {driverMiddleware} = require('./middlewares/driverMiddleware');

// app.use(express.static('./dist'));
app.use(express.json());
app.use(morgan('tiny'));


app.use('/api/auth', authController);
// private
app.use(authMiddleware);
app.use('/api/users/me', usersController);
app.use('/api/trucks', driverMiddleware, trucksController);
app.use('/api/loads', loadController);

// test_hw_3 / nodejs_hw3
const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://yuliayaskevych:jlju@cluster0.ccskx.mongodb.net/nodejs_hw3?retryWrites=true&w=majority',
        {useNewUrlParser: true, useUnifiedTopology: true});
    // , createIndexes: true
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
