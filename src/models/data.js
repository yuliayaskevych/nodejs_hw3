const truckTypes = {
  'SPRINTER': {
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    dimensions: {
      width: 700,
      length: 350,
      height: 200,
    },
    payload: 4000,
  },
};

const stateLoad = ['En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'];

const statusLoad = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

module.exports = {
  truckTypes,
  stateLoad,
  statusLoad,
};
