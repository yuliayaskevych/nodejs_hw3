const mongoose = require('mongoose');

const User = mongoose.model('User', {
  role: {
    type: String,
    required: true,
    enum: ['DRIVER', 'SHIPPER'],
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {
  User,
};
