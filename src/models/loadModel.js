const mongoose = require('mongoose');
const {stateLoad, statusLoad} = require('./data');
const Load = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: [...statusLoad],
    default: 'NEW',
    required: true,
  },
  state: {
    type: String,
    enum: [...stateLoad],
    default: 'En route to Pick Up',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    length: {
      type: Number,
    },
    width: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  logs: {
    type: Array,
    message: String,
    time: Date,
    default: {
      message: 'Load assigned to driver',
      time: new Date(Date.now()),
    },
  },
  // logs: [{
  //   message: {
  //     type: String,
  //     default: 'Load assigned to driver',
  //   },
  //   time: {
  //     type: Date,
  //     default: Date.now(),
  //   },
  // }],
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Load};
